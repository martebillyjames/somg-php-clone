<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Issue_suggestion_form extends Auth_Controller {

	protected $_viewRoot  = 'app/forms/';
	protected $_modelRoot = 'app/';
	protected $_routeRoot = 'app/issue_suggestion_form/';
	protected $_routeDataset = 'app/issue_dataset/';
	protected $_homeRoute = 'list';
	protected $_formID    = 4;

	public function __construct()
	{
		parent::__construct();
		$this->load->model($this->_modelRoot . 'Model_forms', 'm_forms');
		$this->load->model($this->_modelRoot . 'Model_users', 'm_users');
		$this->load->model($this->_modelRoot . 'Model_issue_suggestion_form_data', 'm_issue_suggestion_form_data');
		$this->data['subNavBar']      = 'app/nav_primary';
		$this->data['templateStyle']  = 'sidebar';
	}

	public function index()
	{
		$data 					         = &$this->data;
		$data['pageClass']               = 'hold-transition sidebar-mini';
		$form 	 					 	 = $this->m_forms->getForms($this->_formID)->row_array();

        if(sizeof($form)) {
			$data['primaryView']         = $this->_viewRoot . $form['template_url'];
			$data['pageTitle']           = $form['name'].' :: MTM Reporting';
			$data['form']             	 = $form;
			$data['name']                = $this->session->userdata('name');
			$data['heading']             = $form['name'];
			$data['pageDescription']     = $form['description'];
			$data['breadcrumb']          = array(base_url().'app/forms/list' => 'Forms', '#' => $form['name']);
			$data['formSubmitHandler']   = $this->_routeRoot . 'save';
			$this->load->view('layouts/' . $data['templateStyle'] . '_template', $data);
		} else {
			$this->setFlashMessage('Failure', 'error', 'There is an issue accessing the form, please try again.');
			redirect('forms/list');
		}
	}

	public function edit($issue_suggestion_form_data_id)
	{
		if ($issue_suggestion_form_data_id != '' && $this->authuser->isSuperAdmin()) {
			$data 					        = &$this->data;
			$data['pageClass']              = 'hold-transition sidebar-mini';
			$form 	 					 	= $this->m_forms->getForms($this->_formID)->row_array();

			$data['primaryView']         	= $this->_viewRoot . $form['template_url'];
			$data['pageTitle']           	= $form['name'].' :: MTM Reporting';
			$data['form']             	 	= $form;
			$data['name']                	= $this->session->userdata('name');
			$data['heading']             	= $form['name'];
			$data['pageDescription']     	= $form['description'];
			$data['breadcrumb']          	= array(base_url().'app/forms/list' => 'Forms', base_url().'app/issue_suggestion_form' => $form['name'], '#' => 'Update Data');
			$data['formSubmitHandler']   	= $this->_routeRoot . 'save';
			$data['form_data']   			= $this->m_issue_suggestion_form_data->getFormDataById(false, $issue_suggestion_form_data_id)->row_array();

			$this->load->view('layouts/' . $data['templateStyle'] . '_template', $data);
		} else {
			$this->setFlashMessage('Failure', 'error', 'There is an issue accessing the form, please try again.');
			redirect($this->_routeDataset);
		}
	}

	public function save()
	{
		if ($this->input->server('REQUEST_METHOD') == 'POST') {
			if ($this->input->post('reporting_week')) {
				$reporting_week = explode('-', $this->input->post('reporting_week'));
				$week_start = new DateTime();
				$week_start->setISODate($reporting_week[0], $reporting_week[1]);
				$_POST['reporting_week'] = $week_start->format('Y-m-d');
			}

			if ($this->input->post('type') !== null) {
				$_POST['type'] = "issue";
			} else {
				$_POST['type'] = "suggestion";
			}

			$post = $_POST;

			//Check duplicate in table
			$select = array('ofd.issue_suggestion_form_data_id');
			$where 	= array('ofd.user_id' 		=> $post['user_id'],
						   'ofd.office_id' 		=> $post['office_id'],
						   'ofd.reporting_week' => $post['reporting_week']);

			if (isset($post['issue_suggestion_form_data_id'])) {
				$issue_suggestion_form_data_id	= $post['issue_suggestion_form_data_id'];
				$where['ofd.issue_suggestion_form_data_id !='] = $post['issue_suggestion_form_data_id'];//Skip the same record in case of edit
			}

			if (isset($issue_suggestion_form_data_id)) {
				$response 					= $this->m_issue_suggestion_form_data->updateFormData($issue_suggestion_form_data_id, $post);
			} else {
				$response 					= $this->m_issue_suggestion_form_data->insertFormData($post);
			}

			if ($response) {
				$this->setFlashMessage('Successful!', 'success', 'Forms data has been was saved.');
			} else {
				$this->setFlashMessage('Failure', 'error', 'There was a problem saving your data, please try again.');
			}
		}

		if (isset($issue_suggestion_form_data_id)) {
			redirect($this->_routeDataset);
		} else{
			redirect($this->_routeRoot);
		}
	}
}
/* End of file Issue_suggestion_forms.php */
