
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12 col-sm-12">
<?php
			$attributes = array('method' => 'post', 'id' => $form['template_url']);
			$hidden = array('user_id' => $this->session->userdata('user_id'));
			$reporting_week = '';
			if (isset($form_data['reporting_week'])){
				$date = new DateTime($form_data['reporting_week']);
				$reporting_week = $date->format("Y-W");
			}

			if (isset($form_data['issue_suggestion_form_data_id'])) {
				$hidden['issue_suggestion_form_data_id'] = $form_data['issue_suggestion_form_data_id'];
				$hidden['office_id'] = $form_data['office_id'];
				$hidden['reporting_week'] = $reporting_week;
			}
			echo form_open(base_url().$formSubmitHandler, $attributes, $hidden);?>
			<fieldset>
				<div class="row">
					<div class="col-lg-12">
						<!-- Reporter Information -->
			            <div class="card card-primary card-outline">
			            	<div class="card-header">
			                	<h3 class="card-title">Reporter Information</h3>
			              	</div>
							<!-- /.card-header -->
							<div class="card-body">
								<div class="container">
									<div class="row">
										<div class="col-lg-4 col-sm-12">
											<div class="form-group">
<?php
												$inputName = 'office_id'.((isset($form_data['issue_suggestion_form_data_id'])) ? '_disabled' : '' );
												$locations = array('' => '- Select -');
												foreach ($this->session->userdata('user_offices') as $key => $value) {
													$locations[$key] = $value['name'];
												}
												$attributes = 'class="form-control" id="'.$inputName.'" tabindex="1"'.((isset($form_data['issue_suggestion_form_data_id'])) ? ' readonly="readonly" disabled' : '' ).' required data-rule-required="true" data-msg-required="Please choose your Location."';?>
												<label for="<?php echo $inputName;?>">Choose Location<sup class="text-danger">*</sup></label>
												<?php echo form_dropdown($inputName, $locations, @$form_data['office_id'], $attributes);?>
												<span class="text-danger"><?php echo form_error($inputName);?></span>
											</div>
										</div>

										<div class="col-lg-4 col-sm-12">
											<div class="form-group">
<?php
												$inputName = 'reporting_week'.((isset($form_data['issue_suggestion_form_data_id'])) ? '_disabled' : '' );
												$data = array('type' => 'text',
															  'name' => $inputName,
															  'id' => $inputName,
					        								  'value' => @$reporting_week,
					        								  'class' => 'form-control',
					        								  'tabindex' => '2',
					        								  'required' => 'required',
					        								  'data-rule-required' => 'true',
					        								  'data-msg-required'  => 'Please select Reporting Week.');

												if (isset($form_data['issue_suggestion_form_data_id'])) {
													$data['readonly'] = 'readonly';
													$data['disabled'] = 'disabled';
												}?>
												<label for="<?php echo $inputName;?>">Reporting Week<sup class="text-danger">*</sup></label>
												<?php echo form_input($data);?>
												<span class="text-danger"><?php echo form_error($inputName);?></span>
											</div>
										</div>

									</div>
								</div>
			              	</div>
			              	<!-- /.card-body -->
			            </div>
			            <!--/.card -->
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<!-- Reporter Information -->
			            <div class="card card-primary card-outline">
			            	<div class="card-header">
			                	<h3 class="card-title">Details</h3>
			              	</div>
							<!-- /.card-header -->
							<div class="card-body">
								<div class="container">
									<div class="row">
										<div class="col-lg-4 col-sm-12">
											<div class="form-group">
<?php
												$inputName = 'subject';
												$data = array('type'     => 'text',
															  'name'     => $inputName,
															  'id'       => $inputName,
					        								  'value'    => @$form_data[$inputName],
					        								  'class'    => 'form-control',
					        								  'tabindex' => '3',
					        								  'required' => 'required',
					        								  'data-rule-required' => 'true',
					        								  'data-msg-required'  => 'Please enter Subject.');?>
												<label for="<?php echo $inputName;?>">Subject<sup class="text-danger">*</sup></label>
												<?php echo form_input($data);?>
												<span class="text-danger"><?php echo form_error($inputName);?></span>
											</div>
										</div>

										<div class="col-lg-4 col-sm-12">
											<div class="form-group">
												<label for="<?php echo $inputName;?>">Issue / Suggestion</label><br>
												<input id="type" name="type" type="checkbox" data-on-color="primary" data-off-color="default" data-on-text="Issue" data-off-text="Suggestion" data-size="mini"
													<?php echo ((isset($form_data['issue_suggestion_form_data_id']) && @$form_data['type'] == "issue") ? ' checked="checked"' : '' );?> data-bootstrap-switch tabindex="8">
												<span class="text-danger"><?php echo form_error($inputName);?></span>
											</div>
										</div>

										<div class="col-lg-12 col-sm-12">
											<div class="form-group">
												<?php
												$inputName = 'description';
												$data = array('type' => 'text',
																'name' => $inputName,
																'id' => $inputName,
																'value' => @$form_data[$inputName],
																'class' => 'form-control',
																'tabindex' => '2',
																'required' => 'required',
																'maxlength' => '255',
																'rows' => '3',
																'data-rule-required' => 'true',
																'data-msg-required'  => 'Please enter Description.');?>
												<label for="<?php echo $inputName;?>">Description<sup class="text-danger">*</sup></label>
												<?php echo form_textarea($data);?>
												<span class="text-danger"><?php echo form_error($inputName);?></span>
											</div>
										</div>
									</div>
								</div>
			              	</div>
			              	<!-- /.card-body -->
			            </div>
			            <!--/.card -->
					</div>
				</div>
				<!-- /Patient Encounters -->

				<div class="row">
					<div class="col-lg-12 col-sm-12">
            			<!-- Bootstrap Switch -->
            			<div class="card card-secondary">
              				<div class="card-body text-center">
								<input type="submit" class="btn btn-primary" value="<?php echo ((isset($form_data['issue_suggestion_form_data_id'])) ? 'Update' : 'Save' );?> Data" tabindex="11" />
								<input type="reset" class="btn btn-default" value="Reset Data" tabindex="12" />
              				</div>
            			</div>
            		</div>
            	</div>
			</fieldset>
			<?php echo form_close();?>
		</div>
	</div>
</div>
