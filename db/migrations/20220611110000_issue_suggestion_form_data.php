<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

final class IssueSuggestionFormData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {

		$issue_suggestion_form = $this->table('issue_suggestion_form_data', ['primary_key' => 'issue_suggestion_form_data_id', 'id' => false]);
		$issue_suggestion_form->addColumn('issue_suggestion_form_data_id','integer', ['identity' => true, 'seed' => 1, 'increment' => 1])
			    ->addColumn('user_id','integer', ['null' => false])
			    ->addForeignKey('user_id', 'users', 'user_id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
			    ->addColumn('office_id','integer', ['null' => false])
			    ->addForeignKey('office_id', 'offices', 'office_id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
			    ->addColumn('reporting_week', 'date', ['null' => false])
			    ->addColumn('subject', 'string', 'string', ['null' => false])
			    ->addColumn('type', 'string', 'string', ['null' => false])
			    ->addColumn('description', 'text', ['null' => true])
			    ->addColumn('created_at', 'timestamp', ['null' => false, 'timezone' => true, 'default' => Literal::from('now()')])
			    ->addColumn('updated_at', 'timestamp', ['null' => true, 'timezone' => true])
			    ->addColumn('deleted_at', 'timestamp', ['null' => true, 'timezone' => true])
			    ->addColumn('deleted', 'boolean', ['default' => false])
			    ->create();
    }
}
