<?php

use Phinx\Seed\AbstractSeed;

class IssueSuggestionSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
		// Truncate so this can be rebuilt in demo/dev environments
		$this->execute('TRUNCATE TABLE issue_suggestion_form_data RESTART IDENTITY CASCADE');
		// Truncate so this can be rebuilt in demo/dev environments

        $formData = [
                        [
                            'issue_suggestion_form_data_id' => 1,
                            'user_id' => 2,
                            'office_id' => 1,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#1 Issue",
                            'type' => "issue",
                            'description' => "Issue number 1. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:00:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 2,
                            'user_id' => 3,
                            'office_id' => 2,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#2 Issue",
                            'type' => "issue",
                            'description' => "Issue number 2. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:02:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 3,
                            'user_id' => 3,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#1 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 2. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:03:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 4,
                            'user_id' => 8,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#2 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 2. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:04:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 5,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#3 Issue",
                            'type' => "issue",
                            'description' => "Issue number 3. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:05:00',
                            'deleted' => false
                         ],


                         [
                            'issue_suggestion_form_data_id' => 6,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-12",
                            'subject' => "#4 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 4. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:06:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 7,
                            'user_id' => 5,
                            'office_id' => 1,
                            'reporting_week' => "2022-06-12",
                            'subject' => "#5 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 5. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:05:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 8,
                            'user_id' => 7,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-11",
                            'subject' => "#6 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 6. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:08:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 9,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-13",
                            'subject' => "#7 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 7. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:06:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 10,
                            'user_id' => 7,
                            'office_id' => 2,
                            'reporting_week' => "2022-06-13",
                            'subject' => "#8 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 8. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:07:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 11,
                            'user_id' => 2,
                            'office_id' => 4,
                            'reporting_week' => "2022-06-13",
                            'subject' => "#9 Suggestion",
                            'type' => "suggestion",
                            'description' => "Suggestion number 9. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:08:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 12,
                            'user_id' => 2,
                            'office_id' => 4,
                            'reporting_week' => "2022-06-14",
                            'subject' => "#3 Issue",
                            'type' => "issue",
                            'description' => "Issue number 3. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:01:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 13,
                            'user_id' => 6,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-14",
                            'subject' => "#4 Issue",
                            'type' => "issue",
                            'description' => "Issue number 4. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:01:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 14,
                            'user_id' => 6,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-15",
                            'subject' => "#5 Issue",
                            'type' => "issue",
                            'description' => "Issue number 5. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:09:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 15,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-15",
                            'subject' => "#6 Issue",
                            'type' => "issue",
                            'description' => "Issue number 6. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 11:08:10',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 16,
                            'user_id' => 6,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-16",
                            'subject' => "#6 Issue",
                            'type' => "issue",
                            'description' => "Issue number 6. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:02:10',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 17,
                            'user_id' => 7,
                            'office_id' => 1,
                            'reporting_week' => "2022-06-16",
                            'subject' => "#7 Issue",
                            'type' => "issue",
                            'description' => "Issue number 7. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:02:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 18,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-17",
                            'subject' => "#8 Issue",
                            'type' => "issue",
                            'description' => "Issue number 8. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:03:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 19,
                            'user_id' => 7,
                            'office_id' => 3,
                            'reporting_week' => "2022-06-17",
                            'subject' => "#9 Issue",
                            'type' => "issue",
                            'description' => "Issue number 9. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:04:00',
                            'deleted' => false
                         ],
                         [
                            'issue_suggestion_form_data_id' => 20,
                            'user_id' => 3,
                            'office_id' => 5,
                            'reporting_week' => "2022-06-18",
                            'subject' => "#10 Issue",
                            'type' => "issue",
                            'description' => "Issue number 10. Lorem ipsum. Hello world",
                            'created_at' => '2022-06-11 12:07:00',
                            'deleted' => false
                         ],

                    ];

        $forms = $this->table('issue_suggestion_form_data');
        $forms->insert($formData)->saveData();
    }
}
